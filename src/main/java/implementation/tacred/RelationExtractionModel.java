package implementation.tacred;

import implementation.algorithms.Partition;
import implementation.utils.CollectionsModel;
import implementation.utils.PartitionThread;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.sparql.algebra.Table;
import org.apache.jena.sparql.algebra.TableFactory;
import org.apache.jena.vocabulary.RDF;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

public class RelationExtractionModel {
	private static final String URI_base = "http://example.org/";
	private static final String URI_ids = RelationExtractionModel.URI_base + "id#";
	private static final String URI_tokens = RelationExtractionModel.URI_base + "token#";
	private static final String URI_pos = RelationExtractionModel.URI_base + "pos#";
	private static final String URI_ner = RelationExtractionModel.URI_base + "ner#";
	private static final String URI_deprel = RelationExtractionModel.URI_base + "deprel#";
	private static final String URI_tacred_schema = RelationExtractionModel.URI_base + "tacred/schema#";
	private static final String URI_tacred_relations = RelationExtractionModel.URI_base + "tacred/relation#";

	private final Map<Character, String> PUNCT_CONVERSION_TABLE = new HashMap<>();
	private final Map<String, Map<String, TacredPair>> importedTrainingPairs = new HashMap<>();
	private final Map<String, TacredPair> importedTestPairs = new HashMap<>();
	private final CollectionsModel model;

	private final Map<String, Map<String, Set<String>>> compatibilityRelationTable = new HashMap<>();

	private final boolean pruneDependencyTree;
	private final boolean verbose;
	private final boolean excludeNegativesTraining;
	private final boolean excludeNegativesTest;
	private final int descriptionDepth;
	private int maxComputationTime;

	/**
	 * Create an empty model.
	 */
	public RelationExtractionModel(boolean prunedDependencyTree, boolean verbose, boolean excludeNegativesTraining, boolean excludeNegativesTest, int depth, int computationTime) {
		/* Initialisation */
		this.pruneDependencyTree = prunedDependencyTree;
		this.verbose = verbose;
		this.excludeNegativesTest = excludeNegativesTest;
		this.excludeNegativesTraining = excludeNegativesTraining;
		this.descriptionDepth = depth;
		this.maxComputationTime = computationTime;

		this.model = new CollectionsModel();
		Model rdfModel = this.model.getModel();

		/* Prefixes */
		rdfModel.setNsPrefix("id", RelationExtractionModel.URI_ids);
		rdfModel.setNsPrefix("token", RelationExtractionModel.URI_tokens);
		rdfModel.setNsPrefix("pos", RelationExtractionModel.URI_pos);
		rdfModel.setNsPrefix("ner", RelationExtractionModel.URI_ner);
		rdfModel.setNsPrefix("deprel", RelationExtractionModel.URI_deprel);
		rdfModel.setNsPrefix("tacred_schema", RelationExtractionModel.URI_tacred_schema);
		rdfModel.setNsPrefix("tacred_relation", RelationExtractionModel.URI_tacred_relations);

		/* Punctuation conversion table */
		this.PUNCT_CONVERSION_TABLE.put('.', "_DOT_");
		this.PUNCT_CONVERSION_TABLE.put(',', "%2C");
		this.PUNCT_CONVERSION_TABLE.put(':', "%3A");
		this.PUNCT_CONVERSION_TABLE.put(';', "%3B");
		this.PUNCT_CONVERSION_TABLE.put('?', "%3F");
		this.PUNCT_CONVERSION_TABLE.put('!', "_EXCLAMATIONMARK_");
		this.PUNCT_CONVERSION_TABLE.put('#', "%23");
		this.PUNCT_CONVERSION_TABLE.put('-', "_HYPHEN_");
		this.PUNCT_CONVERSION_TABLE.put('\'', "%22");
		this.PUNCT_CONVERSION_TABLE.put('\"', "%22");
		this.PUNCT_CONVERSION_TABLE.put('`', "%60");
		this.PUNCT_CONVERSION_TABLE.put('@', "%40");
		this.PUNCT_CONVERSION_TABLE.put('>', "%3E");
		this.PUNCT_CONVERSION_TABLE.put('<', "%3C");
		this.PUNCT_CONVERSION_TABLE.put('$', "%24");
		this.PUNCT_CONVERSION_TABLE.put('€', "_EURO_");
		this.PUNCT_CONVERSION_TABLE.put('£', "_POUND_");
		this.PUNCT_CONVERSION_TABLE.put('|', "7C");
		this.PUNCT_CONVERSION_TABLE.put('\\', "%5C");
		this.PUNCT_CONVERSION_TABLE.put('%', "%25");
		this.PUNCT_CONVERSION_TABLE.put('&', "%26");
		this.PUNCT_CONVERSION_TABLE.put('*', "_ASTERISK_");
		this.PUNCT_CONVERSION_TABLE.put('^', "%5E");
		this.PUNCT_CONVERSION_TABLE.put('+', "%2B");
		this.PUNCT_CONVERSION_TABLE.put('/', "%2F");
		this.PUNCT_CONVERSION_TABLE.put('=', "%3D");
		this.PUNCT_CONVERSION_TABLE.put(' ', "%20");
		this.PUNCT_CONVERSION_TABLE.put('{', "%7B");
		this.PUNCT_CONVERSION_TABLE.put('}', "%7D");
		this.PUNCT_CONVERSION_TABLE.put('~', "%7E");
		this.PUNCT_CONVERSION_TABLE.put('[', "%5B");
		this.PUNCT_CONVERSION_TABLE.put(']', "%5D");
	}

	/**
	 * Return the RDF model of this TACRED model.
	 *
	 * @return the RDF model.
	 */
	public CollectionsModel getModel() {
		return model;
	}

	/**
	 * Compute the @link{Table} containing the couples subject-object of the imported sentences
	 *
	 * @return the @link{Table}
	 */
	public Table getTrainingPairTable() {
		Table ret = TableFactory.create();
		for(String relation : this.importedTrainingPairs.keySet()) {
			for(String id : this.importedTrainingPairs.get(relation).keySet()) {
				ret.addBinding(this.importedTrainingPairs.get(relation).get(id).getBinding());
			}
		}
		return ret;
	}

	public List<String> getTrainingPairList() {
		List<String> ret = new ArrayList<>();
		for(String relation : this.importedTrainingPairs.keySet()) {
			ret.addAll(this.importedTrainingPairs.get(relation).keySet());
		}
		ret.sort(Comparator.comparing(String::toString));
		return ret;
	}

	public List<String> getTestPairList() {
		List<String> ret = new ArrayList<>(this.importedTestPairs.keySet());
		ret.sort(Comparator.comparing(String::toString));
		return ret;
	}

	private TacredPair getTrainingPair(String pairId) {
		for(String relation : this.importedTrainingPairs.keySet()) {
			for(String id : this.importedTrainingPairs.get(relation).keySet()) {
				if(id.equals(pairId)) {
					return this.importedTrainingPairs.get(relation).get(id);
				}
			}
		}
		return null;
	}

	public TacredPair getTestPair(String id) {
		return this.importedTestPairs.get(id);
	}

	public Table getCompatibleTrainingPairTable(TacredPair testPair) {
		Table ret = TableFactory.create();
		for(String relation : this.compatibilityRelationTable.get(testPair.getSubjectType()).get(testPair.getObjectType())) {
			for(String id : importedTrainingPairs.get(relation).keySet()) {
				ret.addBinding(this.importedTrainingPairs.get(relation).get(id).getBinding());
			}
		}
		return ret;
	}

	public Set<String> getCompatibleRelations(TacredPair testPair) {
		if(this.compatibilityRelationTable.containsKey(testPair.getSubjectType()) && this.compatibilityRelationTable.get(testPair.getSubjectType()).containsKey(testPair.getObjectType())) {
			return this.compatibilityRelationTable.get(testPair.getSubjectType()).get(testPair.getObjectType());
		} else {
			return new HashSet<>();
		}
	}

	/**
	 * Convert a whole tacred-formatted JSONArray of sentences into a RDF representation that is added to the model.
	 *
	 * @param tacred_json a tacred-formated JSONArray corresponding to a set of sentences to convert.
	 */
	public void importTacredDatabase(JSONArray tacred_json, boolean isTraining) {
		int length = tacred_json.length();
		for(int i = 0; i < length; i++) {
			JSONObject sentence = tacred_json.getJSONObject(i);
			if(verbose) {
				System.out.printf("[%d/%d] Importing: %s (%f%%)\n", i + 1, length, sentence.getString("id"), (i + 1) * 100.0 / length);
			}
			if(((isTraining && this.excludeNegativesTraining) || (!isTraining && this.excludeNegativesTest)) && sentence.getString("relation").equals("no_relation")) {
				if(verbose) {
					System.out.println("No relation: skipping...");
				}
			} else {
				this.importTacredSentence(sentence, isTraining, verbose);
			}
		}
	}

	/**
	 * Add a sentence to the model
	 *
	 * @param tacred_sentence A sentence as a Tacred-formatted JSONObject
	 */
	public void importTacredSentence(JSONObject tacred_sentence, boolean isTraining, boolean verbose) {
		if(this.importedTrainingPairs.containsKey(tacred_sentence.getString("id"))
				|| this.importedTestPairs.containsKey(tacred_sentence.getString("id"))) {
			return;
		}

		List<Statement> statements = new ArrayList<>();

		String sentence_id = tacred_sentence.getString("id");

		JSONArray tokens = tacred_sentence.getJSONArray("token");
		JSONArray pos = tacred_sentence.getJSONArray("stanford_pos");
		JSONArray ner = tacred_sentence.getJSONArray("stanford_ner");
		JSONArray head = tacred_sentence.getJSONArray("stanford_head");
		JSONArray deprel = tacred_sentence.getJSONArray("stanford_deprel");

		int subj_start_idx = tacred_sentence.getInt("subj_start");
		int subj_end_idx = tacred_sentence.getInt("subj_end");
		String subj_type = tacred_sentence.getString("subj_type");
		int obj_start_idx = tacred_sentence.getInt("obj_start");
		int obj_end_idx = tacred_sentence.getInt("obj_end");
		String obj_type = tacred_sentence.getString("obj_type");

		String tacred_rel_str = tacred_sentence.getString("relation");

		/*
		 * Pre-process the JSON to have consistent head values
		 */
		for(int i = 0; i < tokens.length(); i++) {
			int headVal = head.getInt(i) - 1;
			head.put(i, headVal);
		}

		/*
		 * Pre-process the JSON to merge the split named entities in one token
		 */
		ArrayList<Integer> toBeDeleted = new ArrayList<>();
		ArrayList<Integer> namedEntityStarts = new ArrayList<>();
		ArrayList<Integer> namedEntityEnds = new ArrayList<>();

		/* identify the named entities start and end */
		int start = -1;
		String previousNerType = "O";
		for(int i = 0; i < tokens.length(); i++) {
			if(!ner.getString(i).equals("O")) {
				if(previousNerType.equals("O")) {
					previousNerType = ner.getString(i);
					start = i;
				} else if(!previousNerType.equals(ner.getString(i))) {
					namedEntityStarts.add(start);
					namedEntityEnds.add(i);
					previousNerType = ner.getString(i);
					start = i;
				}
			} else {
				if(!previousNerType.equals("O")) {
					namedEntityStarts.add(start);
					namedEntityEnds.add(i);
					previousNerType = "O";
				}
			}
		}
		if(!previousNerType.equals("O")) {
			namedEntityStarts.add(start);
			namedEntityEnds.add(tokens.length());
		}

		/* For each named entity */
		for(int i = 0; i < namedEntityStarts.size(); i++) {
			int root = -1;
			int rootCount = 0;
			int begin = namedEntityStarts.get(i);
			int end = namedEntityEnds.get(i);
			for(int j = begin; j < end; j++) { // Count the number of roots
				int headVal = head.getInt(j);
				if(headVal < begin || headVal >= end) {
					root = j;
					rootCount++;
				}
			}
			if(rootCount >= 2) { // More than one roots: syntactically inconsistent named entity, cannot be handled
				if(isTraining) {
					if(verbose) {
						System.out.println("inconsistent named entities: skipping the sentence...");
					}
					return;
				}
			} else { // Well-formed named entity
				/* Merge the tokens of the entity */
				String mergedTokens = "";
				for(int j = begin; j < end; j++) {
					if(!mergedTokens.equals("")) {
						mergedTokens = mergedTokens.concat("_");
					}
					mergedTokens = mergedTokens.concat(tokens.getString(j));
				}
				tokens.put(root, mergedTokens);
				/* Update head values */
				for(int j = 0; j < tokens.length(); j++) {
					int headVal = head.getInt(j);
					if(headVal >= begin && headVal < end) {
						head.put(j, root);
					}
				}
				/* Update Tacred subjects & objects start & end if needed */
				if(subj_start_idx >= begin && subj_end_idx < end) {
					subj_start_idx = root;
				}
				if(subj_end_idx >= begin && subj_end_idx < end) {
					subj_end_idx = root;
				}
				if(obj_start_idx >= begin && obj_end_idx < end) {
					obj_start_idx = root;
				}
				if(obj_end_idx >= begin && obj_end_idx < end) {
					obj_end_idx = root;
				}
				/* Add non-root tokens to the deletion list */
				for(int j = begin; j < end; j++) {
					if(j != root) {
						toBeDeleted.add(j);
					}
				}
			}
		}

		/* Delete the tokens to be deleted */
		for(int i = 0; i < toBeDeleted.size(); i++) {
			int line = toBeDeleted.get(i);

			/* Delete lines in JSONArrays */
			tokens.remove(line);
			pos.remove(line);
			ner.remove(line);
			head.remove(line);
			deprel.remove(line);

			/* Update head values */
			for(int j = 0; j < tokens.length(); j++) {
				int headVal = head.getInt(j);
				if(headVal > line) {
					head.put(j, headVal - 1);
				}
			}
			if(subj_start_idx > line) {
				subj_start_idx--;
			}
			if(subj_end_idx > line) {
				subj_end_idx--;
			}
			if(obj_start_idx > line) {
				obj_start_idx--;
			}
			if(obj_end_idx > line) {
				obj_end_idx--;
			}

			/* Update lines to be deleted */
			for(int j = i; j < toBeDeleted.size(); j++) {
				int lj = toBeDeleted.get(j);
				if(lj > line) {
					toBeDeleted.set(j, lj - 1);
				}
			}
		}

		/*
		 * Preprocess the data to remove special characters
		 */
		for(int i = 0; i < tokens.length(); i++) {
			tokens.put(i, this.str_preprocessing_tokens(tokens.getString(i)));
			pos.put(i, this.str_preprocessing(pos.getString(i)));
			ner.put(i, this.str_preprocessing(ner.getString(i)));
			deprel.put(i, this.str_preprocessing(deprel.getString(i)));
		}

		/*
		 * Identify subject and object
		 */
		int subjRoot = subj_start_idx;
		if(subj_end_idx != subj_start_idx) {
			int rootCount = 0;
			ArrayList<Integer> roots = new ArrayList<>();
			for(int i = subj_start_idx; i <= subj_end_idx; i++) {
				int val = i;
				while(head.getInt(val) != -1
						&& head.getInt(val) != val
						&& head.getInt(val) >= subj_start_idx
						&& head.getInt(val) <= subj_end_idx) {
					val = head.getInt(val);
				}
				if(!roots.contains(val)) {
					roots.add(val);
					rootCount++;
				}
			}
			if(rootCount > 1) {
				if(isTraining) {
					if(verbose) {
						System.out.println("Split subject: skipping sentence");
					}
					return;
				}
			}
			subjRoot = roots.get(0);
		}
		int objRoot = obj_start_idx;
		if(obj_end_idx != obj_start_idx) {
			int rootCount = 0;
			ArrayList<Integer> roots = new ArrayList<>();
			for(int i = obj_start_idx; i <= obj_end_idx; i++) {
				int val = i;
				while(head.getInt(val) != -1
						&& head.getInt(val) != val
						&& head.getInt(val) >= obj_start_idx
						&& head.getInt(val) <= obj_end_idx) {
					val = head.getInt(val);
				}
				if(!roots.contains(val)) {
					roots.add(val);
					rootCount++;
				}
			}
			if(rootCount > 1) {
				if(isTraining) {
					if(verbose) {
						System.out.println("Split object: skipping sentence");
					}
					return;
				}
			}
			objRoot = roots.get(roots.size() - 1);
		}

		/*
		 * Prune the dependency tree if needed
		 */
		Set<Integer> toBeModeled = new HashSet<>();
		if(this.pruneDependencyTree) {
			/*
			 * Computation of the path between the subject and the object
			 */
			List<Integer> subjAncestors = new ArrayList<>();
			List<Integer> objAncestors = new ArrayList<>();

			int current = subjRoot;
			do {
				subjAncestors.add(current);
				current = head.getInt(current);
			} while(current != -1);

			current = objRoot;
			do {
				objAncestors.add(current);
				current = head.getInt(current);
			} while(current != -1);

			int i = 0;
			int j = 0;
			int commonAncestor = -1;
			while(commonAncestor == -1 && i < subjAncestors.size()) {
				while(commonAncestor == -1 && j < objAncestors.size()) {
					if(subjAncestors.get(i).equals(objAncestors.get(j))) {
						commonAncestor = subjAncestors.get(i);
					} else {
						j++;
					}
				}
				if(commonAncestor == -1) {
					i++;
					j = 0;
				}
			}

			/*
			 * Selection of the tokens to add to the model
			 */
			i = 0;
			while(subjAncestors.get(i) != commonAncestor) {
				toBeModeled.add(subjAncestors.get(i));
				i++;
			}
			j = 0;
			while(objAncestors.get(j) != commonAncestor) {
				toBeModeled.add(objAncestors.get(j));
				j++;
			}

			toBeModeled.add(commonAncestor);
			if(head.getInt(commonAncestor) != -1) {
				toBeModeled.add(head.getInt(commonAncestor));
			}

			Set<Integer> neighbourhood = new HashSet<>();
			for(i = 0; i < tokens.length(); i++) {
				if(!neighbourhood.contains(i) && !toBeModeled.contains(i) && toBeModeled.contains(head.getInt(i))) {
					neighbourhood.add(i);
				}
			}
			toBeModeled.addAll(neighbourhood);
		} else {
			// If not pruned, all tokens must be conserved
			for(int i = 0; i < tokens.length(); i++) {
				toBeModeled.add(i);
			}
		}

		/*
		 * Deletion of the punctuation tokens which are leaf
		 */
		Set<Integer> punctLeafTokens = new HashSet<>();
		// Select the punctuation tokens
		for(int token : toBeModeled) {
			if(deprel.getString(token).equals("punct") && token != subjRoot && token != objRoot) {
				punctLeafTokens.add(token);
			}
		}
		// Remove the punctuations that are not leaves from the set of punctuations
		for(int token : toBeModeled) {
			int parent = head.getInt(token);
			punctLeafTokens.remove(parent);
		}
		// Remove the punctuation leaves from the set of conserved tokens
		for(int token : punctLeafTokens) {
			toBeModeled.remove(token);
		}

		/*
		 * Add token, POSTag & NERTag (if exists) as types to each token id
		 */
		Map<Integer, Resource> id_resources = new HashMap<>();

		for(int i : toBeModeled) {
			String id = sentence_id + "_" + i + "_" + this.str_preprocessing_tokens(tokens.getString(i));
			id_resources.put(i, this.model.createResource(RelationExtractionModel.URI_ids + id));

			String token_str = tokens.getString(i);
			Resource token_resource = this.model.createResource(RelationExtractionModel.URI_tokens + token_str);
			Statement stmt = this.model.createStatement(id_resources.get(i), RDF.type, token_resource);
			statements.add(stmt);

			String pos_str = pos.getString(i);
			Resource pos_resource = this.model.createResource(RelationExtractionModel.URI_pos + pos_str);
			stmt = this.model.createStatement(id_resources.get(i), RDF.type, pos_resource);
			statements.add(stmt);

			if(!(ner.getString(i)).equals("O")) {
				Resource ner_resource = this.model.createResource(RelationExtractionModel.URI_ner + ner.getString(i));
				stmt = this.model.createStatement(id_resources.get(i), RDF.type, ner_resource);
				statements.add(stmt);
			}
		}

		/*
		 * Add syntactic relations between token ids
		 */
		for(int i : toBeModeled) {
			String deprel_name = deprel.getString(i);
			int parent = head.getInt(i);
			if(toBeModeled.contains(parent) && !deprel_name.equals("ROOT")) {
				Property deprel_property = this.model.createProperty(RelationExtractionModel.URI_deprel, "has_" + deprel_name);
				int head_index = head.getInt(i);
				Statement stmt = model.createStatement(id_resources.get(head_index), deprel_property, id_resources.get(i));
				statements.add(stmt);
			}
		}

		/*
		 * Identifies TACRED subject & object
		 */
		Resource subject_res = id_resources.get(subjRoot);
		Resource object_res = id_resources.get(objRoot);

		TacredPair pair = new TacredPair(subject_res, object_res, tacred_rel_str, sentence_id, subj_type, obj_type);

		if(isTraining) {
			if(!this.importedTrainingPairs.containsKey(tacred_rel_str)) {
				this.importedTrainingPairs.put(tacred_rel_str, new HashMap<>());
			}
			this.importedTrainingPairs.get(tacred_rel_str).put(sentence_id, pair);
		} else {
			this.importedTestPairs.put(sentence_id, pair);
		}
		this.model.add(statements);

		/*
		 * Add the relation to the compatibility table
		 */
		if(isTraining) {
			if(!this.compatibilityRelationTable.containsKey(subj_type)) {
				this.compatibilityRelationTable.put(subj_type, new HashMap<>());
			}
			if(!this.compatibilityRelationTable.get(subj_type).containsKey(obj_type)) {
				this.compatibilityRelationTable.get(subj_type).put(obj_type, new HashSet<>());
			}
			this.compatibilityRelationTable.get(subj_type).get(obj_type).add(tacred_rel_str);
		}
	}

	private String str_preprocessing_tokens(String str) {
		String str_ret = "";
		for(int i = 0; i < str.length(); i++) {
			if(this.PUNCT_CONVERSION_TABLE.containsKey(str.charAt(i))) {
				str_ret = str_ret.concat(this.PUNCT_CONVERSION_TABLE.get(str.charAt(i)));
			} else {
				str_ret = str_ret.concat(String.valueOf(Character.toLowerCase(str.charAt(i))));
			}
		}
		return str_ret;
	}

	private String str_preprocessing(String str) {
		String str_ret = "";
		for(int i = 0; i < str.length(); i++) {
			if(this.PUNCT_CONVERSION_TABLE.containsKey(str.charAt(i))) {
				str_ret = str_ret.concat(this.PUNCT_CONVERSION_TABLE.get(str.charAt(i)));
			} else {
				str_ret = str_ret.concat(String.valueOf(str.charAt(i)));
			}
		}
		return str_ret;
	}

	public JSONObject processTestExample(String id) throws InterruptedException {
		TacredPair pair = this.getTestPair(id);
		String gold = pair.getRelation();
		String guess = null;
		JSONObject results = null;

		Set<String> compatibleRelations = this.getCompatibleRelations(pair);

		if(compatibleRelations.size() == 0) {
			guess = "no_relation";
			results = new JSONObject();
		} else if(compatibleRelations.size() == 1) {
			guess = compatibleRelations.iterator().next();
			results = new JSONObject();
		} else {
			/*
			 * Computation of the CNN for this example
			 */
			Table table = getCompatibleTrainingPairTable(pair);
			Partition partition = new Partition(this.getModel(), pair.getUris(), this.descriptionDepth, table);
			AtomicBoolean cut = new AtomicBoolean(false);
			PartitionThread thread = new PartitionThread(partition, cut);
			thread.start();
			thread.join(this.maxComputationTime * 1000);
			cut.set(true);
			thread.join(1000);
			if(thread.isAlive()) {
				System.err.println("interrupting thread");
				thread.stop();
			}

			/*
			 * Computation of the scores
			 */
			Map<String, Double> scores = new HashMap<>();
			results = new JSONObject(new JSONTokener(partition.toJson()));

			JSONArray clusters = results.getJSONArray("clusters");

			int nbClusters = clusters.length();

			for(int i = 0; i < nbClusters; i++) {
				JSONObject cluster = clusters.getJSONObject(i);
				int extensionalDistance = cluster.getInt("extensional Distance");

				JSONArray answers = cluster.getJSONArray("answers");
				for(int j = 0; j<answers.length(); j++) {
					JSONArray ans = answers.getJSONArray(j);
					String pairId = ans.getString(0).replaceAll("id:", "").replaceAll("_.*", "");
					String relation = this.getTrainingPair(pairId).getRelation();
					if(scores.containsKey(relation)) {
						scores.put(relation, scores.get(relation) + Math.exp(-extensionalDistance));
					} else {
						scores.put(relation, Math.exp(-extensionalDistance));
					}
				}
			}

			/*
			 * Add the scores to the result
			 */
			List<String> relations = new ArrayList<>(scores.keySet());
			relations.sort((s1, s2) -> (scores.get(s2).compareTo(scores.get(s1))));
			guess = relations.get(0);

			for(String rel : relations) {
				JSONObject jsonScore = new JSONObject();
				jsonScore.put("relation", rel);
				jsonScore.put("score", scores.get(rel));
				results.append("scores", jsonScore);
			}
		}

		results.put("gold", gold);
		results.put("guess", guess);
		results.put("id", id);
		for(String relation : compatibleRelations) {
			results.append("compatible_relations", relation);
		}

		return results;
	}
}
