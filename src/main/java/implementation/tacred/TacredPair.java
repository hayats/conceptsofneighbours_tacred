package implementation.tacred;

import org.apache.jena.graph.Node;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.sparql.core.Var;
import org.apache.jena.sparql.engine.binding.Binding;
import org.apache.jena.sparql.engine.binding.BindingFactory;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class TacredPair {
	private Resource subject;
	private Resource object;
	private String relation;
	private String id;
	private String subjectType = null;
	private String objectType = null;

	public TacredPair(Resource subject, Resource object, String relation, String id) {
		this.subject = subject;
		this.object = object;
		this.relation = relation;
		this.id = id;
	}

	public TacredPair(Resource subject, Resource object, String relation, String id, String subjectType, String objectType) {
		this(subject, object, relation, id);
		this.subjectType = subjectType;
		this.objectType = objectType;
	}

	public Resource getSubject() {
		return this.subject;
	}

	public Resource getObject() {
		return this.object;
	}

	public String getId() {
		return this.id;
	}

	public String getRelation() {
		return this.relation;
	}

	public String getSubjectType() {
		return subjectType;
	}

	public String getObjectType() {
		return objectType;
	}

	public Binding getBinding() {
		Var neighbor0 = Var.alloc("Neighbor_0");
		Var neighbor1 = Var.alloc("Neighbor_1");
		Node subject = this.subject.asNode();
		Node object = this.object.asNode();

		return BindingFactory.binding(BindingFactory.binding(neighbor0, subject), neighbor1, object);
	}

	public JSONObject toJson() {
		JSONObject serialized = new JSONObject();
		serialized.put("id", this.id);
		serialized.put("subject", this.subject.toString());
		serialized.put("object", this.object.toString());
		serialized.put("relation", this.relation);
		return serialized;
	}

	public List<String> getUris() {
		List<String> ret = new ArrayList<>();
		ret.add(this.subject.getURI());
		ret.add(this.object.getURI());
		return ret;
	}

	public Binding getSubjectBinding() {
		Var neighbor = Var.alloc("Neighbor_0");
		Node subject = this.subject.asNode();

		return BindingFactory.binding(neighbor, subject);
	}
}
