package implementation.tacred;

import implementation.utils.CollectionsModel;
import implementation.utils.JSONable;
import org.apache.jena.rdf.model.*;
import org.apache.jena.reasoner.ReasonerRegistry;
import org.apache.jena.sparql.algebra.Table;
import org.apache.jena.sparql.algebra.TableFactory;
import org.apache.jena.vocabulary.RDF;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.*;
import java.util.*;

/**
 * Model for representing TACRED data into a RDF graph
 *
 * @author Hugo Ayats
 */
public class TacredModel implements JSONable {
	public static final String URI_base = "http://example.org/";
	public static final String URI_ids = TacredModel.URI_base + "id#";
	public static final String URI_tokens = TacredModel.URI_base + "token#";
	public static final String URI_pos = TacredModel.URI_base + "pos#";
	public static final String URI_ner = TacredModel.URI_base + "ner#";
	public static final String URI_deprel = TacredModel.URI_base + "deprel#";
	public static final String URI_tacred_schema = TacredModel.URI_base + "tacred/schema#";
	public static final String URI_tacred_relations = TacredModel.URI_base + "tacred/relation#";
	public static final String URI_type = TacredModel.URI_base + "tacred/type#";

	private final Map<Character, String> PUNCT_CONVERSION_TABLE;

	private final Map<String, TacredPair> importedTrainingPairs;
	private final Map<String, TacredPair> importedTestPairs;
	private CollectionsModel model;

	private int nbEntities = 0;
	private int nbSplitEntities = 0;
	private int nbSubjects = 0;
	private int nbSplitSubjects = 0;
	private int nbObjects = 0;
	private int nbSplitObjects = 0;
	private int nbAutoreferentEntities = 0;
	private int nbSplitSentences = 0;

	/**
	 * Create an empty model.
	 */
	public TacredModel() {
		/* Initialisation */
		this.model = new CollectionsModel();
		Model rdfModel = this.model.getModel();
		this.importedTrainingPairs = new HashMap<>();
		this.importedTestPairs = new HashMap<>();

		/* Prefixes */
		rdfModel.setNsPrefix("id", TacredModel.URI_ids);
		rdfModel.setNsPrefix("token", TacredModel.URI_tokens);
		rdfModel.setNsPrefix("pos", TacredModel.URI_pos);
		rdfModel.setNsPrefix("ner", TacredModel.URI_ner);
		rdfModel.setNsPrefix("deprel", TacredModel.URI_deprel);
		rdfModel.setNsPrefix("tacred_schema", TacredModel.URI_tacred_schema);
		rdfModel.setNsPrefix("tacred_relation", TacredModel.URI_tacred_relations);

		/* Punctuation conversion table */
		this.PUNCT_CONVERSION_TABLE = new HashMap<>();
		this.PUNCT_CONVERSION_TABLE.put('.', "_DOT_");
		this.PUNCT_CONVERSION_TABLE.put(',', "%2C");
		this.PUNCT_CONVERSION_TABLE.put(':', "%3A");
		this.PUNCT_CONVERSION_TABLE.put(';', "%3B");
		this.PUNCT_CONVERSION_TABLE.put('?', "%3F");
		this.PUNCT_CONVERSION_TABLE.put('!', "_EXCLAMATIONMARK_");
		this.PUNCT_CONVERSION_TABLE.put('#', "%23");
		this.PUNCT_CONVERSION_TABLE.put('-', "_HYPHEN_");
		this.PUNCT_CONVERSION_TABLE.put('\'', "%22");
		this.PUNCT_CONVERSION_TABLE.put('\"', "%22");
		this.PUNCT_CONVERSION_TABLE.put('`', "%60");
		this.PUNCT_CONVERSION_TABLE.put('@', "%40");
		this.PUNCT_CONVERSION_TABLE.put('>', "%3E");
		this.PUNCT_CONVERSION_TABLE.put('<', "%3C");
		this.PUNCT_CONVERSION_TABLE.put('$', "%24");
		this.PUNCT_CONVERSION_TABLE.put('€', "_EURO_");
		this.PUNCT_CONVERSION_TABLE.put('£', "_POUND_");
		this.PUNCT_CONVERSION_TABLE.put('|', "7C");
		this.PUNCT_CONVERSION_TABLE.put('\\', "%5C");
		this.PUNCT_CONVERSION_TABLE.put('%', "%25");
		this.PUNCT_CONVERSION_TABLE.put('&', "%26");
		this.PUNCT_CONVERSION_TABLE.put('*', "_ASTERISK_");
		this.PUNCT_CONVERSION_TABLE.put('^', "%5E");
		this.PUNCT_CONVERSION_TABLE.put('+', "%2B");
		this.PUNCT_CONVERSION_TABLE.put('/', "%2F");
		this.PUNCT_CONVERSION_TABLE.put('=', "%3D");
		this.PUNCT_CONVERSION_TABLE.put(' ', "%20");
		this.PUNCT_CONVERSION_TABLE.put('{', "%7B");
		this.PUNCT_CONVERSION_TABLE.put('}', "%7D");
		this.PUNCT_CONVERSION_TABLE.put('~', "%7E");
		this.PUNCT_CONVERSION_TABLE.put('[', "%5B");
		this.PUNCT_CONVERSION_TABLE.put(']', "%5D");
	}

	/**
	 * Create a TacredModel from serialized files
	 * @param modelFis stream containing the serialized RDF model
	 * @param tacredModelFis stream containing the serialized TACRED model
	 */
	public TacredModel(FileInputStream modelFis, FileInputStream tacredModelFis) {
		this();

		InfModel rdfModel = ModelFactory.createInfModel(ReasonerRegistry.getRDFSReasoner(), ModelFactory.createDefaultModel());
		rdfModel.read(modelFis, null, "TTL");
		this.model = new CollectionsModel(rdfModel);
		JSONObject serializedConv = new JSONObject(new JSONTokener(tacredModelFis));
		JSONArray serializedPairs = serializedConv.getJSONArray("importedPairs");
		for(int i = 0; i < serializedPairs.length(); i++) {
			JSONObject serPair = serializedPairs.getJSONObject(i);
			this.importedTrainingPairs.put(serPair.getString("id"), new TacredPair(
					ResourceFactory.createResource(serPair.getString("subject")),
					ResourceFactory.createResource(serPair.getString("object")),
					serPair.getString("relation"),
					serPair.getString("id")));
		}
	}

	/**
	 * Return the RDF model of this TACRED model.
	 *
	 * @return the RDF model.
	 */
	public CollectionsModel getModel() {
		return model;
	}

	/**
	 * Compute the @link{Table} containing the couples subject-object of the imported sentences
	 *
	 * @return the @link{Table}
	 */
	public Table getTrainingPairTable() {
		Table ret = TableFactory.create();
		for(String key : this.importedTrainingPairs.keySet()) {
			ret.addBinding(this.importedTrainingPairs.get(key).getBinding());
		}

		return ret;
	}

	public Table getTrainingSubjectTable() {
		Table ret = TableFactory.create();
		for(String key : this.importedTrainingPairs.keySet()) {
			ret.addBinding(this.importedTrainingPairs.get(key).getSubjectBinding());
		}

		return ret;
	}

	public List<String> getTrainingPairList() {
		List<String> ret = new ArrayList<>(this.importedTrainingPairs.keySet());
		ret.sort(Comparator.comparing(String::toString));
		return ret;
	}

	public List<String> getTestPairList() {
		List<String> ret = new ArrayList<>(this.importedTestPairs.keySet());
		ret.sort(Comparator.comparing(String::toString));
		return ret;
	}

	/**
	 * Convert a tacred-formatted JSONObject corresponding to a
	 * sentence into facts that are added to the RDF model.
	 *
	 * @param tacred_sentence A tacred-formatted JSONObject to convert in RDF representation.
	 */
	public void importTacredSentence(JSONObject tacred_sentence, boolean isTraining, boolean pruned, boolean verbose) {
		if(this.importedTrainingPairs.containsKey(tacred_sentence.getString("id"))
				|| this.importedTestPairs.containsKey(tacred_sentence.getString("id"))) {
			return;
		}

		List<Statement> statements = new ArrayList<>();

		String sentence_id = tacred_sentence.getString("id");

		JSONArray tokens = tacred_sentence.getJSONArray("token");
		JSONArray pos = tacred_sentence.getJSONArray("stanford_pos");
		JSONArray ner = tacred_sentence.getJSONArray("stanford_ner");
		JSONArray head = tacred_sentence.getJSONArray("stanford_head");
		JSONArray deprel = tacred_sentence.getJSONArray("stanford_deprel");

		int subj_start_idx = tacred_sentence.getInt("subj_start");
		int subj_end_idx = tacred_sentence.getInt("subj_end");
		String subj_type = tacred_sentence.getString("subj_type");
		int obj_start_idx = tacred_sentence.getInt("obj_start");
		int obj_end_idx = tacred_sentence.getInt("obj_end");
		String obj_type = tacred_sentence.getString("obj_type");

		String tacred_rel_str = tacred_sentence.getString("relation");

		/*
		 * Pre-process the JSON to have consistent head values
		 */
		for (int i = 0; i < tokens.length(); i++) {
			int headVal = head.getInt(i) - 1;
			head.put(i, headVal);
		}

		/*
		 * Pre-process the JSON to merge the split named entities in one token
		 */
		ArrayList<Integer> toBeDeleted = new ArrayList<>();
		ArrayList<Integer> namedEntityStarts = new ArrayList<>();
		ArrayList<Integer> namedEntityEnds = new ArrayList<>();

		/* identify the named entities start and end */
		int start = -1;
		String previousNerType = "O";
		for (int i = 0; i < tokens.length(); i++) {
			if (!ner.getString(i).equals("O")) {
				if (previousNerType.equals("O")) {
					previousNerType = ner.getString(i);
					start = i;
				} else if (!previousNerType.equals(ner.getString(i))) {
					namedEntityStarts.add(start);
					namedEntityEnds.add(i);
					previousNerType = ner.getString(i);
					start = i;
				}
			} else {
				if (!previousNerType.equals("O")) {
					namedEntityStarts.add(start);
					namedEntityEnds.add(i);
					previousNerType = "O";
				}
			}
		}
		if (!previousNerType.equals("O")) {
			namedEntityStarts.add(start);
			namedEntityEnds.add(tokens.length());
		}

		/* For each named entity */
		for (int i = 0; i < namedEntityStarts.size(); i++) {
			this.nbEntities++;
			int root = -1;
			int rootCount = 0;
			int begin = namedEntityStarts.get(i);
			int end = namedEntityEnds.get(i);
			for (int j = begin; j < end; j++) { // Count the number of roots
				int headVal = head.getInt(j);
				if (headVal < begin || headVal >= end) {
					root = j;
					rootCount++;
				}
			}
			if (rootCount == 0) { // No root: auto-referent named entity, cannot be handled
				this.nbAutoreferentEntities++;
			} else if (rootCount >= 2) { // More than one roots: syntactically inconsistent named entity, cannot be handled
				this.nbSplitEntities++;
				if(isTraining && pruned) {
					if(verbose) {
						System.out.println("inconsistent named entities: skipping the sentence...");
					}
					return;
				}
			} else { // Well-formed named entity
				/* Merge the tokens of the entity */
				String mergedTokens = "";
				for (int j = begin; j < end; j++) {
					if (!mergedTokens.equals("")) {
						mergedTokens = mergedTokens.concat("_");
					}
					mergedTokens = mergedTokens.concat(tokens.getString(j));
				}
				tokens.put(root, mergedTokens);
				/* Update head values */
				for (int j = 0; j < tokens.length(); j++) {
					int headVal = head.getInt(j);
					if (headVal >= begin && headVal < end) {
						head.put(j, root);
					}
				}
				/* Update Tacred subjects & objects start & end if needed */
				if (subj_start_idx >= begin && subj_end_idx < end) {
					subj_start_idx = root;
				}
				if (subj_end_idx >= begin && subj_end_idx < end) {
					subj_end_idx = root;
				}
				if (obj_start_idx >= begin && obj_end_idx < end) {
					obj_start_idx = root;
				}
				if (obj_end_idx >= begin && obj_end_idx < end) {
					obj_end_idx = root;
				}
				/* Add non-root tokens to the deletion list */
				for (int j = begin; j < end; j++) {
					if (j != root) {
						toBeDeleted.add(j);
					}
				}
			}
		}

		/* Delete the tokens to be deleted */
		for (int i = 0; i < toBeDeleted.size(); i++) {
			int line = toBeDeleted.get(i);

			/* Delete lines in JSONArrays */
			tokens.remove(line);
			pos.remove(line);
			ner.remove(line);
			head.remove(line);
			deprel.remove(line);

			/* Update head values */
			for (int j = 0; j < tokens.length(); j++) {
				int headVal = head.getInt(j);
				if (headVal > line) {
					head.put(j, headVal - 1);
				}
			}
			if (subj_start_idx > line) {
				subj_start_idx--;
			}
			if (subj_end_idx > line) {
				subj_end_idx--;
			}
			if (obj_start_idx > line) {
				obj_start_idx--;
			}
			if (obj_end_idx > line) {
				obj_end_idx--;
			}

			/* Update lines to be deleted */
			for (int j = i; j < toBeDeleted.size(); j++) {
				int lj = toBeDeleted.get(j);
				if (lj > line) {
					toBeDeleted.set(j, lj - 1);
				}
			}
		}

		/*
		 * Preprocess the data to remove special characters
		 */
		for (int i = 0; i < tokens.length(); i++) {
			tokens.put(i, this.str_preprocessing_tokens(tokens.getString(i)));
			pos.put(i, this.str_preprocessing(pos.getString(i)));
			ner.put(i, this.str_preprocessing(ner.getString(i)));
			deprel.put(i, this.str_preprocessing(deprel.getString(i)));
		}

		/*
		 * Identify subject and object, and prune the dependency tree if needed
		 */
		boolean splitSubj = false;
		int subjRoot = subj_start_idx;
		this.nbSubjects++;
		if (subj_end_idx != subj_start_idx) {
			int rootCount = 0;
			ArrayList<Integer> roots = new ArrayList<>();
			for (int i = subj_start_idx; i <= subj_end_idx; i++) {
				int val = i;
				while (head.getInt(val) != -1
						&& head.getInt(val) != val
						&& head.getInt(val) >= subj_start_idx
						&& head.getInt(val) <= subj_end_idx) {
					val = head.getInt(val);
				}
				if (!roots.contains(val)) {
					roots.add(val);
					rootCount++;
				}
			}
			if(rootCount > 1) {
				splitSubj = true;
				this.nbSplitSubjects++;
				if(isTraining && pruned) {
					if(verbose) {
						System.out.println("Split subject: skipping sentence");
					}
					return;
				}
			}
			subjRoot = roots.get(0);
		}
		boolean splitObj = false;
		int objRoot = obj_start_idx;
		this.nbObjects++;
		if(obj_end_idx != obj_start_idx) {
			int rootCount = 0;
			ArrayList<Integer> roots = new ArrayList<>();
			for (int i = obj_start_idx; i <= obj_end_idx; i++) {
				int val = i;
				while (head.getInt(val) != -1
						&& head.getInt(val) != val
						&& head.getInt(val) >= obj_start_idx
						&& head.getInt(val) <= obj_end_idx) {
					val = head.getInt(val);
				}
				if (!roots.contains(val)) {
					roots.add(val);
					rootCount++;
				}
			}
			if(rootCount > 1) {
				splitObj = true;
				this.nbSplitObjects++;
				if(isTraining && pruned) {
					if(verbose) {
						System.out.println("Split object: skipping sentence");
					}
					return;
				}
			}
			objRoot = roots.get(roots.size() - 1);
		}

		Set<Integer> toBeModeled = new HashSet<>();
		if(pruned) {
			/*
			 * Computation of the path between the subject and the object
			 */
			List<Integer> subjAncestors = new ArrayList<>();
			List<Integer> objAncestors = new ArrayList<>();

			int current = subjRoot;
			do {
				subjAncestors.add(current);
				current = head.getInt(current);
			} while(current != -1);

			current = objRoot;
			do {
				objAncestors.add(current);
				current = head.getInt(current);
			} while(current != -1);

			int i = 0;
			int j = 0;
			int commonAncestor = -1;
			while(commonAncestor == -1 && i < subjAncestors.size()) {
				while(commonAncestor == -1 && j < objAncestors.size()) {
					if(subjAncestors.get(i).equals(objAncestors.get(j))) {
						commonAncestor = subjAncestors.get(i);
					} else {
						j++;
					}
				}
				if(commonAncestor == -1) {
					i++;
					j = 0;
				}
			}

			/*
			 * Selection of the tokens to add to the model
			 */
			i = 0;
			while(subjAncestors.get(i) != commonAncestor) {
				toBeModeled.add(subjAncestors.get(i));
				i++;
			}
			j = 0;
			while(objAncestors.get(j) != commonAncestor) {
				toBeModeled.add(objAncestors.get(j));
				j++;
			}

			toBeModeled.add(commonAncestor);
			if(head.getInt(commonAncestor) != -1) {
				toBeModeled.add(head.getInt(commonAncestor));
			}

			Set<Integer> neighbourhood = new HashSet<>();
			for(i = 0; i < tokens.length(); i++) {
				if(!neighbourhood.contains(i) && !toBeModeled.contains(i) && toBeModeled.contains(head.getInt(i))) {
					neighbourhood.add(i);
				}
			}
			toBeModeled.addAll(neighbourhood);
		} else {
			// If not pruned, all tokens must be conserved
			for(int i = 0; i < tokens.length(); i++) {
				toBeModeled.add(i);
			}
		}

		/*
		 * Deletion of the punctuation tokens which are leaf
		 */
		Set<Integer> punctLeafTokens = new HashSet<>();
		// Select the punctuation tokens
		for(int token : toBeModeled) {
			if(deprel.getString(token).equals("punct") && token != subjRoot && token != objRoot) {
				punctLeafTokens.add(token);
			}
		}
		// Remove the punctuations that are not leaves from the set of punctuations
		for(int token : toBeModeled) {
			int parent = head.getInt(token);
			punctLeafTokens.remove(parent);
		}
		// Remove the punctuation leaves from the set of conserved tokens
		for(int token : punctLeafTokens) {
			toBeModeled.remove(token);
		}

		/*
		 * Add token, POSTag & NERTag (if exists) as types to each token id
		 */
		Map<Integer, Resource> id_resources = new HashMap<>();

		for(int i : toBeModeled) {
			String id = sentence_id + "_" + i + "_" + this.str_preprocessing_tokens(tokens.getString(i));
			id_resources.put(i, this.model.createResource(TacredModel.URI_ids + id));

			String token_str = tokens.getString(i);
			Resource token_resource = this.model.createResource(TacredModel.URI_tokens + token_str);
			Statement stmt = this.model.createStatement(id_resources.get(i), RDF.type, token_resource);
			statements.add(stmt);

			String pos_str = pos.getString(i);
			Resource pos_resource = this.model.createResource(TacredModel.URI_pos + pos_str);
			stmt = this.model.createStatement(id_resources.get(i), RDF.type, pos_resource);
			statements.add(stmt);

			if(!(ner.getString(i)).equals("O")) {
				Resource ner_resource = this.model.createResource(TacredModel.URI_ner + ner.getString(i));
				stmt = this.model.createStatement(id_resources.get(i), RDF.type, ner_resource);
				statements.add(stmt);
			}
		}

		/*
		 * Add syntactic relations between token ids
		 */
		for(int i : toBeModeled) {
			String deprel_name = deprel.getString(i);
			int parent = head.getInt(i);
			if(toBeModeled.contains(parent) && !deprel_name.equals("ROOT")) {
				Property deprel_property = this.model.createProperty(TacredModel.URI_deprel, "has_" + deprel_name);
				int head_index = head.getInt(i);
				Statement stmt = model.createStatement(id_resources.get(head_index), deprel_property, id_resources.get(i));
				statements.add(stmt);
			}
		}

		/*
		 * Identifies TACRED subject, object and relation
		 */
		Resource subject_res = id_resources.get(subjRoot);
		Resource subject_type = this.model.createResource(TacredModel.URI_type + subj_type);
		Statement stmt = this.model.createStatement(subject_res, RDF.type, subject_type);
		statements.add(stmt);

		Resource object_res = id_resources.get(objRoot);
		Resource object_type = this.model.createResource(TacredModel.URI_type + obj_type);
		stmt = this.model.createStatement(object_res, RDF.type, object_type);
		statements.add(stmt);

		if(splitObj || splitSubj) {
			this.nbSplitSentences++;
		}

		TacredPair pair = new TacredPair(subject_res, object_res, tacred_rel_str, sentence_id);

		if(isTraining) {
			this.importedTrainingPairs.put(sentence_id, pair);
		} else {
			this.importedTestPairs.put(sentence_id,pair);
		}
		this.model.add(statements);
	}


	/**
	 * Convert a whole tacred-formatted JSONArray of sentences into a RDF representation that is added to the model.
	 *
	 * @param tacred_json a tacred-formated JSONArray corresponding to a set of sentences to convert.
	 * @param verbose     set to <i>true</i> to have a verbose import
	 */
	public void importTacredDatabase(JSONArray tacred_json, boolean isTraining, boolean pruned, int relations_only, boolean verbose) {
		int length = tacred_json.length();
		for(int i = 0; i < length; i++) {
			JSONObject sentence = tacred_json.getJSONObject(i);
			if(verbose) {
				System.out.printf("[%d/%d] Importing: %s (%f%%)\n", i + 1, length, sentence.getString("id"), (i + 1) * 100.0 / length);
			}
			if(relations_only == 1 && sentence.getString("relation").equals("no_relation")) {
				if(verbose) {
					System.out.println("No relation: skipping...");
				}
			} else if(relations_only == -1 && !sentence.getString("relation").equals("no_relation")) {
				if(verbose) {
					System.out.println("Relation: skipping...");
				}
			} else{
				this.importTacredSentence(sentence, isTraining, pruned, verbose);
			}
		}
	}

	private String str_preprocessing_tokens(String str) {
		String str_ret = "";
		for (int i = 0; i < str.length(); i++) {
			if (this.PUNCT_CONVERSION_TABLE.containsKey(str.charAt(i))) {
				str_ret = str_ret.concat(this.PUNCT_CONVERSION_TABLE.get(str.charAt(i)));
			} else {
				str_ret = str_ret.concat(String.valueOf(Character.toLowerCase(str.charAt(i))));
			}
		}
		return str_ret;
	}

	private String str_preprocessing(String str) {
		String str_ret = "";
		for(int i = 0; i < str.length(); i++) {
			if(this.PUNCT_CONVERSION_TABLE.containsKey(str.charAt(i))) {
				str_ret = str_ret.concat(this.PUNCT_CONVERSION_TABLE.get(str.charAt(i)));
			} else {
				str_ret = str_ret.concat(String.valueOf(str.charAt(i)));
			}
		}
		return str_ret;
	}

	public String toJson() { // Warning: does not serialize this.rdfModel
		JSONObject serialized = new JSONObject();

		/* serializing this.importedTrainingPairs */
		for(String key : this.importedTrainingPairs.keySet()) {
			serialized.append("trainingPairs", this.importedTrainingPairs.get(key).toJson());
		}

		/* serializing this.importedTestPairs */
		for(String key : this.importedTestPairs.keySet()) {
			serialized.append("testPairs", this.importedTestPairs.get(key).toJson());
		}

		return serialized.toString();
	}

	/**
	 * Serialize this model in two files: one containing the RDF model (in TTL format), one
	 * containing the data specific to the TACRED converter (JSON format).
	 * @param rdfFos a @link{FileOutputStream} in which to serialize the RDF model
	 * @param tacredFos a @link{FileOutputStream} in which to serialize the TACRED model
	 */
	public void serializeToFiles(FileOutputStream rdfFos, FileOutputStream tacredFos) {
		Writer writer = new PrintWriter(tacredFos);
		try {
			this.model.getModel().write(rdfFos, "TTL");
			rdfFos.close();
			writer.write(this.toJson());
			writer.close();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}

	public int getNbEntities() {
		return nbEntities;
	}

	public int getNbSplitEntities() {
		return nbSplitEntities;
	}

	public int getNbSubjects() {
		return nbSubjects;
	}

	public int getNbSplitSubjects() {
		return nbSplitSubjects;
	}

	public int getNbObjects() {
		return nbObjects;
	}

	public int getNbSplitObjects() {
		return nbSplitObjects;
	}

	public int getNbAutoreferentEntities() {
		return nbAutoreferentEntities;
	}

	public int getNbSplitSentences() {
		return nbSplitSentences;
	}

	public TacredPair getTrainingPair(String id) {
		return this.importedTrainingPairs.get(id);
	}

	public TacredPair getTestPair(String id) {
		return this.importedTestPairs.get(id);
	}

	public int getNbrImportedTrainingSentences() {
		return this.importedTrainingPairs.size();
	}
}
