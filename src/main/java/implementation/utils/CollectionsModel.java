package implementation.utils;

import org.apache.jena.graph.Node;
import org.apache.jena.rdf.model.*;
import org.apache.jena.rdf.model.impl.ResourceImpl;
import org.apache.jena.rdf.model.impl.SelectorImpl;
import org.apache.jena.reasoner.ReasonerRegistry;
import org.apache.jena.sparql.algebra.Table;
import org.apache.jena.sparql.core.Var;
import org.apache.jena.sparql.syntax.Element;
import org.apache.jena.vocabulary.RDFS;

import java.util.*;

/**
 * A representation of an RDF Graph using several {@link HashMap} to accelerate accesses
 *
 * @author nk-fouque
 * @author Hugo Ayats
 */
public class CollectionsModel {

	private InfModel infModel;

	private Map<String, Map<Property, List<RDFNode>>> triples = new HashMap<>();
	private Map<String, Map<Property, List<RDFNode>>> triplesReversed = new HashMap<>();

	private Map<Element, Table> ans = new HashMap<>();
	private Map<String, Var> keys = new HashMap<>();
	private int nextKey = 1;
	private Map<Element, Integer> depth = new HashMap<>();

	/**
	 * Create a CollectionsModel based on a previous @link{InfModel}
	 * @param mdInf the model to use as a base for the new CollectionModel
	 */
	public CollectionsModel(InfModel mdInf) {
		this.infModel = mdInf;
		Model graph = this.infModel.getRawModel();


		StmtIterator iter = infModel.listStatements();
		iter.forEachRemaining(stmt -> {
			Map<Property, List<RDFNode>> propertiesFrom = triples.computeIfAbsent(stmt.getSubject().toString(), m -> new HashMap<>());
			List<RDFNode> thatPropertyFrom = propertiesFrom.computeIfAbsent(stmt.getPredicate(), (l) -> new ArrayList<>());
			thatPropertyFrom.add(stmt.getObject());
			Map<Property, List<RDFNode>> propertiesTo = triplesReversed.computeIfAbsent(stmt.getObject().toString(), m -> new HashMap<>());
			List<RDFNode> thatPropertyTo = propertiesTo.computeIfAbsent(stmt.getPredicate(), (l) -> new ArrayList<>());
			thatPropertyTo.add(stmt.getSubject());
		});

		iter = infModel.listStatements(new SelectorImpl(null, RDFS.range, (RDFNode) null));
		iter.forEachRemaining(statement -> graph.add(statement));

		iter = infModel.listStatements(new SelectorImpl(null, RDFS.domain, (RDFNode) null));
		iter.forEachRemaining(statement -> graph.add(statement));
	}

	/**
	 * Create an empty CollectionsModel.
	 */
	public CollectionsModel() {
		this(ModelFactory.createInfModel(ReasonerRegistry.getRDFSReasoner(), ModelFactory.createDefaultModel()));
	}

	/**
	 * Add a statement to the model
	 * @param statement the @link{Statement} to add
	 * @return this
	 */
	public CollectionsModel add(Statement statement) { // FIXME trouver un moyen d'ajouter les statements déduits
		this.infModel.add(statement);

		Map<Property, List<RDFNode>> propertiesFrom = triples.computeIfAbsent(statement.getSubject().toString(), m -> new HashMap<>());
		List<RDFNode> thatPropertyFrom = propertiesFrom.computeIfAbsent(statement.getPredicate(), (l) -> new ArrayList<>());
		thatPropertyFrom.add(statement.getObject());
		Map<Property, List<RDFNode>> propertiesTo = triplesReversed.computeIfAbsent(statement.getObject().toString(), m -> new HashMap<>());
		List<RDFNode> thatPropertyTo = propertiesTo.computeIfAbsent(statement.getPredicate(), (l) -> new ArrayList<>());
		thatPropertyTo.add(statement.getSubject());

		return this;
	}

	public CollectionsModel add(List<Statement> statements) {
		statements.forEach(s -> this.add(s));
		return this;
	}

	/**
	 * Create a Resource attached to the model
	 * @param s the URI of the resource
	 * @return the Resource created
	 */
	public Resource createResource(String s) {
		return infModel.createResource(s);
	}

	/**
	 * Create a Property attached to the model
	 * @param prefix prefix of the Property's URI
	 * @param uri suffix of the Property's URI
	 * @return the created Property
	 */
	public Property createProperty(String prefix, String uri) {
		return this.infModel.createProperty(prefix,uri);
	}

	/**
	 * create a Statement attached to the model
	 * @param s the subject of the statement
	 * @param p the property of the statement
	 * @param o the object of the statement
	 * @return the created Statement
	 */
	public Statement createStatement(Resource s, Property p, RDFNode o) {
		return this.infModel.createStatement(s,p,o);
	}

	/**
	 * Create a Literal attached to the model
	 * @param s the name of the Literal
	 * @return the created Literal
	 */
	public RDFNode createLiteral(String s) {
		return this.infModel.createLiteral(s);
	}

	/**
	 * Remove the a list of statements form the model
	 * @param statements the list of Statements to remove
	 */
	public void remove(List<Statement> statements) {
		infModel.remove(statements);

		//FIXME trouver un moyen de supprimer les statements déduits
		for(Statement statement: statements) {
			Map<Property, List<RDFNode>> propertiesFrom = triples.get(statement.getSubject().toString());
			List<RDFNode> thatPropertyFrom = propertiesFrom.get(statement.getPredicate());
			thatPropertyFrom.remove(statement.getObject());
			if(thatPropertyFrom.isEmpty()) {
				propertiesFrom.remove(statement.getPredicate());
				if(propertiesFrom.isEmpty()) {
					triples.remove(statement.getSubject());
				}
			}
			Map<Property, List<RDFNode>> propertiesTo = triplesReversed.get(statement.getObject().toString());
			List<RDFNode> thatPropertyTo = propertiesTo.get(statement.getPredicate());
			thatPropertyTo.remove(statement.getSubject());
			if(thatPropertyFrom.isEmpty()) {
				propertiesFrom.remove(statement.getPredicate());
				if(propertiesFrom.isEmpty()) {
					triples.remove(statement.getObject());
				}
			}

		}
	}

	/**
	 * @return The RDF Graph in its Jena {@link Model} form
	 */
	public Model getRawModel() {
		return infModel.getRawModel();
	}

	/**
	 * @return The RDF Graph with applied inference reasoning
	 */
	public InfModel getModel() {
		return infModel;
	}

	/**
	 * @return An iterator on resources that are subclasses of the one in parameter
	 */
	public NodeIterator subClassesOf(Node node) {
		return getRawModel().listObjectsOfProperty(new ResourceImpl(node.getURI()), RDFS.subClassOf);
	}

	/**
	 * @return An iterator on resources that are subproperties of the one in parameter
	 */
	public NodeIterator subPropertiesOf(Node node) {
		return getRawModel().listObjectsOfProperty(new ResourceImpl(node.getURI()), RDFS.subPropertyOf);
	}

	/**
	 * @return An iterator on Statements that have the one in parameter as subject
	 */
	public StmtIterator triplesFrom(Resource resource) {
		return getModel().listStatements(new SelectorImpl(resource, null, (RDFNode) null));
	}

	/**
	 * @return An iterator on Statements that have the one in parameter as object
	 */
	public StmtIterator triplesTo(RDFNode node) {
		return getModel().listStatements(new SelectorImpl(null, null, node));
	}

	/**
	 * Same as {@link #triplesFrom(Resource)} but without inference reasoning
	 */
	public StmtIterator simpleTriplesFrom(Resource resource) {
		return getRawModel().listStatements(new SelectorImpl(resource, null, (RDFNode) null));
	}

	/**
	 * Same as {@link #triplesTo(RDFNode)} (Resource)} but without inference reasoning
	 */
	public StmtIterator simpleTriplesTo(RDFNode node) {
		return getRawModel().listStatements(new SelectorImpl(null, null, node));
	}

	public Map<String, Map<Property, List<RDFNode>>> getTriples() {
		return triples;
	}

	public Map<String, Map<Property, List<RDFNode>>> getTriplesReversed() {
		return triplesReversed;
	}

	/**
	 * @param element
	 * @return All the answers to a query containing the element as only selector
	 */
	public Table ans(Element element) {
		return ans.getOrDefault(element, null);
	}

	/**
	 * Adds an element and the corresponding table of answers to the model
	 */
	public void addAns(Element element, Table table) {
		ans.put(element, table);
	}

	/**
	 * Getter for the keys
	 * @return the keys
	 */
	public Map<String, Var> getKeys() {
		return keys;
	}

	public Var varKey(String uri) {
		if(keys.containsKey(uri)) {
			return keys.get(uri);
		} else {
			Var key = keys.computeIfAbsent(uri, var -> Var.alloc("x" + nextKey));
			nextKey++;
			return key;
		}
	}

	public int getDepth(Element element) {
		return depth.get(element);
	}

	public int setDepth(Element element, int i) {
		depth.putIfAbsent(element, i);
		return depth.get(element);
	}

	@Override
	public String toString() {
		return ("\n\n" + triples + "\n\n" + triplesReversed);
	}

	public String shortform(String s) {
		String res = getRawModel().shortForm(s);
		if(res.contains("^^")) {
			String[] temp = res.split("\\^\\^", 2);
			String val = temp[0];
			String type = getRawModel().shortForm(temp[1]);
			res = (val + "^^" + type);
		}
		if(res.equals(s)) {
			String[] temp = s.split("/");
			res = temp[temp.length - 1];
		}

		return res;
	}

	/**
	 * Reset the variables used in the computation of the concepts of neighbours
	 */
	public void reset() {
		this.keys = new HashMap<>();
		this.ans = new HashMap<>();
		this.nextKey = 1;
		this.depth = new HashMap<>();

	}
}