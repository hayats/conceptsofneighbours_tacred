package implementation.utils;

import implementation.tacred.TacredModel;
import org.json.JSONArray;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.file.Paths;
import java.util.List;

public class TacredThread extends Thread {
	private JSONArray trainingJSON;
	private String outputPath;
	private JSONArray testJSON;
	private int idx_start;
	private int idx_end;
	private int depth;
	private int computationTimePerSentence;

	public TacredThread(
			JSONArray trainingJSON,
			String outputPath,
			JSONArray testJSON,
			int idx_start,
			int idx_end,
			int conceptDepth,
			int computationTimePerSentence,
			String name) {
		super(name);
		this.trainingJSON = trainingJSON;
		this.outputPath = outputPath;
		this.testJSON = testJSON;
		this.idx_start = idx_start;
		this.idx_end = idx_end;
		this.depth = conceptDepth;
		this.computationTimePerSentence = computationTimePerSentence;
	}


	@Override
	public void run() {
		/* Load the converter */
		System.out.println(Thread.currentThread().getName() + " loading model...");
		TacredModel tacredModel = new TacredModel();
		tacredModel.importTacredDatabase(this.trainingJSON, true, false, 1, true);
		tacredModel.importTacredDatabase(this.testJSON, false, false, 1, true);
		this.trainingJSON = null;
		this.testJSON = null;

		/* Compute concepts of neighbors for each sentence */
		List<String> testIds = tacredModel.getTestPairList();
		for(int i = this.idx_start; i < this.idx_end; i++) {
			String sentenceId = testIds.get(i);
			System.out.println(Thread.currentThread().getName() + " computation of neighbours for sentence " + (i + 1-this.idx_start) + "/" + (this.idx_end-this.idx_start));
			String partitionJson = null;
			try {
				partitionJson = TacredUtils.sentencePartitioning(sentenceId, tacredModel, this.depth, this.computationTimePerSentence).toJson();
			} catch(InterruptedException e) {
				e.printStackTrace();
			}

			if(partitionJson != null) {
				System.out.println(Thread.currentThread().getName() + " saving neighbors...");
				try {
					Writer writer = new OutputStreamWriter(new FileOutputStream(Paths.get(this.outputPath, sentenceId + ".json").toString()));
					writer.write(partitionJson);
					writer.close();
				} catch(IOException e) {
					e.printStackTrace();
				}
			}
		}
		System.out.println(Thread.currentThread().getName() + " terminated");
	}
}
