package implementation.utils;

import implementation.algorithms.Partition;
import implementation.tacred.TacredModel;
import implementation.tacred.TacredPair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Collection of useful methods in the use of concepts of neighbours on the TACRED dataset
 */
public class TacredUtils {

	/**
	 *
	 * @param pairId The sentence as represented in the JSON TACRED database
	 * @param tacredModel the TacredModel on which to work
	 * @param depth the max depth of the computed neighbours
	 * @param executionTime the max time (in seconds) for the computation of the concepts of neighbours
	 * @return a JSON-style string representing the computed concepts of neighbours
	 * @throws InterruptedException
	 */
	public static Partition sentencePartitioning(String pairId, TacredModel tacredModel, int depth, int executionTime) throws InterruptedException {
		TacredPair pair = tacredModel.getTestPair(pairId);
		Partition partition = TacredUtils.createPartition(tacredModel, pair, depth);
		AtomicBoolean cut = new AtomicBoolean(false);
		PartitionThread thread = new PartitionThread(partition, cut);
		thread.start();
		thread.join(executionTime*1000);
		cut.set(true);
		thread.join(5000);
		if(thread.isAlive()) {
			System.err.println("interrupting thread");
			thread.stop();
		}
		return partition;
	}

	private static Partition createPartition(TacredModel model, TacredPair pair, int depth) {
		return new Partition(model.getModel(), pair.getUris(), depth, model.getTrainingPairTable());
	}

	public static Map<String, Double> computeScores(TacredModel model, JSONObject partition) {
		Map<String, Double> ret = new HashMap<>();

		JSONArray clusters = partition.getJSONArray("clusters");

		int nbClusters = clusters.length();

		for(int i = 0; i < nbClusters; i++) {
			JSONObject cluster = clusters.getJSONObject(i);
			int extensionalDistance = cluster.getInt("extensional Distance");

			JSONArray answers = cluster.getJSONArray("answers");
			for(int j = 0; j<answers.length(); j++) {
				JSONArray ans = answers.getJSONArray(j);
				String id = ans.getString(0).replaceAll("id:", "").replaceAll("_.*", "");
				String relation = model.getTrainingPair(id).getRelation();
				if(ret.containsKey(relation)) {
					ret.put(relation, ret.get(relation) + Math.exp(-extensionalDistance));
				} else {
					ret.put(relation, Math.exp(-extensionalDistance));
				}
			}
		}

		return ret;
	}
}
