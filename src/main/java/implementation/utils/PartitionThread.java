package implementation.utils;

import implementation.algorithms.Partition;

import javax.net.ssl.ManagerFactoryParameters;
import java.lang.management.ManagementFactory;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * A basic thread used to run the partitionning alogrithm for the computation of Concepts od Neighbours
 *
 * @author Hugo Ayats
 */
public class PartitionThread extends Thread {
	private Partition partition;
	private AtomicBoolean cut;
	private long execTime;

	public PartitionThread(Partition partition, AtomicBoolean cut) {
		super();
		this.partition = partition;
		this.cut = cut;
	}

	@Override
	public void run() {
		this.partition.completePartitioning(cut);
		this.execTime = ManagementFactory.getThreadMXBean().getThreadCpuTime(this.getId());
	}

	public long getExecTime() {
		return execTime;
	}
}
