package implementation.utils;

public interface JSONable {
	String toJson();
}
