package implementation.utils;

public class PartitionStats {
	public double nbClusters = 0;
	public double nbNearClusters = 0;
	public double nbNearElements = 0;
	public double avgEltPerClusters = 0;
	public double avgEltPerNearClusters = 0;
	public double nbElements = 0;

	public PartitionStats() {
		super();
	}

	public String toString() {
		return "Number of clusters: " + this.nbClusters + "\n" +
				"Number of elements: " + this.nbElements + "\n" +
				"Avg. elements per clusters: " + this.avgEltPerClusters + "\n" +
				"Number of narrow clusters: " + this.nbNearClusters + "\n" +
				"Number of elements in narrow clusters: " + this.nbNearElements + "\n" +
				"Avg. elements per narrow clusters: " + this.avgEltPerNearClusters;
	}
}
