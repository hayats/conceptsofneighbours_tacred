package implementation;

import implementation.utils.TacredThread;
import org.apache.commons.cli.*;
import org.json.JSONArray;
import org.json.JSONTokener;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class GenerateNeighbours {
	public static void main(String[] args) throws FileNotFoundException {
		NeighborsImplementation.myLogsLevels("off");

		Options options = new Options();

		Option modelOption = new Option("i", "input", true, "Path to the Tacred input json file");
		modelOption.setRequired(true);
		options.addOption(modelOption);

		Option tacredFileOption = new Option("t", "tacred", true, "Path to the JSON representation of the TACRED database of sentences to process");
		tacredFileOption.setRequired(true);
		options.addOption(tacredFileOption);

		Option outputOption = new Option("o", "output", true, "Path to the dir where to put the output files. If not specify, the local folder is used.");
		options.addOption(outputOption);

		Option threadOption = new Option("n", "number-threads", true, "Number of threads to run simultaneously. If not specify, a unique thread is run.");
		options.addOption(threadOption);

		Option timeOption = new Option("T", "max-time-per-sentence", true, "Maximum time (in seconds) of computation of concepts of neighbours for each sentence. If not specified, there is no limit time");
		options.addOption(timeOption);

		Option depthOption = new Option("d", "max-depth", true, "Max depth of the computed concepts of neighbours.");
		depthOption.setRequired(true);
		options.addOption(depthOption);


		CommandLineParser parser = new DefaultParser();
		HelpFormatter helpFormatter = new HelpFormatter();
		CommandLine cmd = null;

		try {
			cmd = parser.parse(options, args);
		} catch(ParseException e) {
			System.out.println(e.getMessage());
			helpFormatter.printHelp("GenerateNeighbours", options);

			System.exit(1);
		}

		JSONArray inputJSON = new JSONArray(new JSONTokener(new FileInputStream(cmd.getOptionValue("input"))));
		String tacredFilePath = cmd.getOptionValue("tacred");

		String outputDirPath;
		if(cmd.hasOption("output")) {
			outputDirPath = cmd.getOptionValue("output");
		} else {
			outputDirPath = "";
		}

		int nbThreads;
		if(cmd.hasOption("number-threads")) {
			nbThreads = Integer.parseInt(cmd.getOptionValue("number-threads"));
		} else {
			nbThreads = 1;
		}

		int maxTime;
		if(cmd.hasOption("max-time-per-sentence")) {
			maxTime = Integer.parseInt(cmd.getOptionValue("max-time-per-sentence"));
		} else {
			maxTime = 0;
		}

		int maxDepth = Integer.parseInt(cmd.getOptionValue("max-depth"));

		FileInputStream tacredFis;
		JSONArray tacredArray;
		try {
			tacredFis = new FileInputStream(tacredFilePath);
			tacredArray = new JSONArray(new JSONTokener(tacredFis));
			tacredFis.close();
		} catch(IOException e) {
			System.err.println("TACRED file not found");
			e.printStackTrace();
			return;
		}

		int totalLength = tacredArray.length();

		TacredThread[] threads = new TacredThread[nbThreads];
		for(int i = 0; i < nbThreads; i++) {
			int begin = (int)(((float)i) / nbThreads * totalLength);
			int end = (int)((float)(i + 1) / nbThreads * totalLength);
			threads[i] = new TacredThread(inputJSON, outputDirPath, tacredArray, begin, end, maxDepth, maxTime, "tacred-thread-" + i);
		}

		for(Thread thread : threads) {
			System.out.println("Starting thread: " + thread.getName());
			thread.start();
		}

		for(Thread thread : threads) {
			try {
				thread.join();
			} catch(InterruptedException e) {
				e.printStackTrace();
			}
		}

	}
}
