package implementation;

import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class StatsOnScores {
	public static void main(String[] args) throws FileNotFoundException {
		String pathToDir = args[0];

		File f = new File(pathToDir);
		String[] pathNames = f.list();

		List<Double> scores = new ArrayList<>();

		for(String path : pathNames) {
			JSONObject file = new JSONObject(new JSONTokener(new FileInputStream(pathToDir + "/" + path)));
			if(file.getString("gold").equals(file.getJSONArray("scores").getJSONObject(0).getString("relation"))) {
				double score = file.getJSONArray("scores").getJSONObject(0).getDouble("score");
				scores.add(score);
			}
		}

		scores.sort(Double::compareTo);

		double median = scores.get((scores.size() / 2) - 1);
		double min = scores.get(0);
		double max = scores.get(scores.size() - 1);
		double quartile1 = scores.get((scores.size() / 4) - 1);
		double quartile3 = scores.get((scores.size() * 3 / 4) - 1);
		double avg = 0;
		int n = scores.size();
		for(double s : scores) {
			avg += s;
		}
		avg /= scores.size();

		System.out.printf("N = %d\nMin = %f\nQ1 = %f\nMedian = %f\nQ3 = %f\nMax = %f\nAvg = %f\n", n, min, quartile1, median, quartile3, max, avg);
	}
}
