package implementation;

import implementation.tacred.TacredModel;
import org.json.JSONArray;
import org.json.JSONTokener;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class NamedEntitiesStats {
	public static void main(String[] args) throws FileNotFoundException {
		String inputFile = args[0];

		TacredModel tacredModel = new TacredModel();

		FileInputStream fis = new FileInputStream(inputFile);
		JSONArray input = new JSONArray(new JSONTokener(fis));

		tacredModel.importTacredDatabase(input, true, false, 0, true);

		System.out.printf("\n\n\nNamed entities: %d\nSplit named entities: %d (%f)\nAutoreferentNamedEntities: %d (%f)\n\n", tacredModel.getNbEntities(), tacredModel.getNbSplitEntities(), ((float) tacredModel.getNbSplitEntities()) / tacredModel.getNbEntities(), tacredModel.getNbAutoreferentEntities(), ((float) tacredModel.getNbAutoreferentEntities()) / tacredModel.getNbEntities());
		System.out.printf("Subjects: %d\nSplit Subjects: %d (%f)\n\n", tacredModel.getNbSubjects(), tacredModel.getNbSplitSubjects(), ((float) tacredModel.getNbSplitSubjects()) / tacredModel.getNbSubjects());
		System.out.printf("Objects: %d\nSplit Objects: %d (%f)\n\n", tacredModel.getNbObjects(), tacredModel.getNbSplitObjects(), ((float) tacredModel.getNbSplitObjects()) / tacredModel.getNbObjects());
		System.out.printf("Sentences with split object and/or subject: %d (%f)\n", tacredModel.getNbSplitSentences(), ((float) tacredModel.getNbSplitSentences()) / tacredModel.getNbrImportedTrainingSentences());
	}
}
