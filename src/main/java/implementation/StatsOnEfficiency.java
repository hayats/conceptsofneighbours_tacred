package implementation;

import implementation.algorithms.Partition;
import implementation.tacred.TacredModel;
import implementation.tacred.TacredPair;
import implementation.utils.PartitionThread;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.sparql.algebra.Table;
import org.json.JSONArray;
import org.json.JSONTokener;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class StatsOnEfficiency {
	public static void main(String[] args) throws FileNotFoundException, InterruptedException {
		NeighborsImplementation.myLogsLevels("off");
		String trainingPath = args[0];
		String testPath = args[1];

		TacredModel model = new TacredModel();

		model.importTacredDatabase(new JSONArray(new JSONTokener(new FileInputStream(trainingPath))), true, true, 1, true);

		model.importTacredDatabase(new JSONArray(new JSONTokener(new FileInputStream(testPath))), false, true, 1, true);

		List<String> pairs = model.getTestPairList();

		Table initTable = model.getTrainingSubjectTable();

		double interruptedThread = 0;
		double nbTotalClusters = 0;
		double totalTime = 0;
		ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();
		int i = 0;
		int j = 0;
		for(int k = 0; k < pairs.size() * 0.05; k++) {
			String id = pairs.get(k);
			j++;
			if(j == 10) {
				j = 0;
				System.gc();
			}
			i++;
			System.out.printf("[%d/%d] Sentence id: %s\n", i, (int) (pairs.size() * 0.05), id);
			TacredPair pair = model.getTestPair(id);
			Resource subject = pair.getSubject();
			List<String> target = new ArrayList<>();
			target.add(subject.getURI());
			Partition partition = new Partition(model.getModel(), target, 50, initTable);

			AtomicBoolean cut = new AtomicBoolean(false);

			PartitionThread thread = new PartitionThread(partition, cut);

			thread.start();
			thread.join(10000);
			cut.set(true);
			if(thread.isAlive()) {
				thread.join(5000);
			}
			if(thread.isAlive()) {
				thread.stop();
				interruptedThread++;
				System.out.println("Thread interrupted");
			}

			double computTime = ((double) thread.getExecTime()) / 1000000000;
			if(computTime == 0) {
				computTime = ((double) threadMXBean.getThreadCpuTime(thread.getId())) / 1000000000;
			}
			int nbClusters = (partition.getClusters().size() + partition.getNeighbors().size());
			totalTime += computTime;
			nbTotalClusters += nbClusters;
			System.out.printf("%d concepts, %f seconds\n", nbClusters, computTime);
		}

		System.out.printf("Avg nb clusters : %f\nAvg computation time per cluster : %f\nProportion of interrupted computations : %f\n", nbTotalClusters / (pairs.size()*0.05), totalTime / (pairs.size()*0.05), interruptedThread / pairs.size());
	}
}
