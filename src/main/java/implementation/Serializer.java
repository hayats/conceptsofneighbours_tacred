package implementation;

import implementation.tacred.TacredModel;
import org.json.JSONArray;
import org.json.JSONTokener;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class Serializer {
	public static void main(String[] args) throws FileNotFoundException {
		String trainingDatabase = args[0];
		String testDatabase = args[1];
		String modelOutput = args[2];
		String tacredOutput = args[3];

		TacredModel model = new TacredModel();

		model.importTacredDatabase(new JSONArray(new JSONTokener(new FileInputStream(trainingDatabase))), true, false, 1,true);
		model.importTacredDatabase(new JSONArray(new JSONTokener(new FileInputStream(testDatabase))), false, false, 1, true);

		System.out.println("serializing...");
		model.serializeToFiles(new FileOutputStream(modelOutput), new FileOutputStream(tacredOutput));
	}
}
