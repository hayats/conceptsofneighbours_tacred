package implementation.algorithms.matchTree;

import implementation.utils.CollectionsModel;
import implementation.utils.TableUtils;
import org.apache.jena.sparql.algebra.Table;
import org.apache.jena.sparql.core.Var;
import org.apache.jena.sparql.syntax.Element;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * The root node of a match-tree
 *
 * @author nk-fouque
 * @author Hugo Ayats
 */
public class MatchTreeRoot extends MatchTreeNode {
	public static Logger logger = Logger.getLogger(MatchTreeNode.class);

	/**
	 * Base Constructor
	 *
	 * @param top   A list with the variables to be returned in the answer, in the basic implementation, only one Var is relevant here
	 * @param colMd The RDF Graph to work in
	 * @param initializationTable a table containing the tuples to use initialize partitioning. If null, generate a exhaustive table with all the entities instead
	 */
	public MatchTreeRoot(List<Var> top, CollectionsModel colMd, Table initializationTable) {
		super();
		this.element = null;
		this.varE = new HashSet<>();
		this.D = new ArrayList<>(top);

		this.matchSet = Objects.requireNonNullElseGet(initializationTable, () -> TableUtils.generateAllTuples(top, colMd.getRawModel().listSubjects()));

		this.delta = new ArrayList<>(top);

		this.children = new HashSet<>();
	}

	/**
	 * Base Constructor
	 *
	 * @param top   A list with the variables to be returned in the answer.
	 * @param colMd The RDF Graph to work in
	 */
	public MatchTreeRoot(List<Var> top, CollectionsModel colMd) {
		this(top, colMd, null);
	}

	/**
	 * Constructor for copies
	 *
	 * @param other The MatchTreeRoot to copy
	 */
	public MatchTreeRoot(MatchTreeNode other) {
		super();
		this.element = null;
		this.varE = new HashSet<>();
		this.D = other.getD();

		this.matchSet = other.getMatchSet();
		this.delta = new ArrayList<>(other.getDelta());

		this.children = new HashSet<>(other.getChildren());

	}

	@Override
	public String toString() {
		return super.toString(0);
	}

	/**
	 * @return A Table containing all the proper answers for the Cluster
	 */
	public Table getMatchSet() {
		return matchSet;
	}

	/**
	 * Applies The Lazy Joins Algorithm to insert a new node
	 *
	 * @param element   The element defining the new node
	 * @param colMd     The RDF Graph to work in
	 * @param varPprime The variables already defined in the cluster
	 * @return A copy of this tree with the new element
	 * @see MatchTreeNode#lazyJoin(MatchTreeRoot, MatchTreeNode)
	 */
	public MatchTreeRoot lazyJoin(Element element, CollectionsModel colMd, Set<Var> varPprime) {
		MatchTreeNode newnode = new MatchTreeNode(element, colMd, varPprime);
		LazyJoin res = this.lazyJoin(this, newnode);
		return new MatchTreeRoot(res.copy);
	}
}
