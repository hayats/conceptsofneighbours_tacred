package implementation.algorithms;

import implementation.algorithms.matchTree.MatchTreeRoot;
import implementation.utils.*;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.sparql.algebra.Table;
import org.apache.jena.sparql.core.Var;
import org.apache.jena.sparql.syntax.Element;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.io.ByteArrayOutputStream;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Class for the first algorithm described in the article
 *
 * @author nk-fouque
 * @author Hugo Ayats
 */
public class Partition implements JSONable {
	private static Logger logger = Logger.getLogger(Partition.class);
	private CollectionsModel graph;
	private ArrayList<Cluster> clusters;
	private List<Cluster> neighbors;
	private int depth;
	private int nextCluster;
	private List<String> target;

	/**
	 * Used for numbering clusters, makes debugging easier
	 *
	 * @return the id of the next cluster
	 */
	private int getNextClusterId() {
		this.nextCluster++;
		return this.nextCluster;
	}

	/**
	 * @return A list containing all the clusters that are still partitionable
	 * Should always be empty at the end of the algorithm
	 */
	public List<Cluster> getClusters() {
		return this.clusters;
	}

	/**
	 * @return A list containing all the neighbors concepts, cluster that have been fully partitioned
	 */
	public List<Cluster> getNeighbors() {
		return this.neighbors;
	}

	/**
	 * The RDF model used by this partition
	 *
	 * @see CollectionsModel
	 */
	public CollectionsModel getGraph() {
		return this.graph;
	}

	/**
	 * @param colMd            A preexisting CollectionsModelSet in which to describe the node and search for its neighbors
	 * @param uriTarget        The list of full length uri of the nodes to describe
	 * @param descriptionDepth The depth of initial node description (the maximum distance a node can be to the initial node to describe it)
	 */
	public Partition(CollectionsModel colMd, List<String> uriTarget, int descriptionDepth, Table initTable) {
		this.target = uriTarget;
		this.nextCluster = 0;
		this.graph = colMd;

		this.graph.reset();
		int i = 0;
		List<Var> neighbor = new ArrayList<>();
		for(String uri : this.target) {
			neighbor.add(Var.alloc("Neighbor_" + i));
			this.graph.getKeys().put(uri, neighbor.get(i));
			i++;
		}

		Set<Element> nodeDescriptions = ElementUtils.describeNodeList(uriTarget, colMd, 1);

		this.clusters = new ArrayList<>();
		this.clusters.add(new Cluster(nodeDescriptions, neighbor, colMd, getNextClusterId(), this.getArity(), initTable));

		this.neighbors = new ArrayList<>();

		this.depth = descriptionDepth;
	}

	/**
	 * @param colMd            A preexisting CollectionsModelSet in which to describe the node and search for its neighbors
	 * @param uriTarget        The list of full length uri of the nodes to describe
	 * @param descriptionDepth The depth of initial node description (the maximum distance a node can be to the initial node to describe it)
	 */
	public Partition(CollectionsModel colMd, List<String> uriTarget, int descriptionDepth) {
		this(colMd,uriTarget,descriptionDepth,null);
	}

	/**
	 * Applies one iteration of the Partition algorithm
	 *
	 * @return false if the partitioning is over, true if it can still be iterated
	 */
	public boolean oneStepPartitioning() throws PartitionException, OutOfMemoryError {
		Cluster c;
		try {
			c = this.clusters.remove(0);
		} catch(IndexOutOfBoundsException ex) {
			throw new PartitionException("Tried to resume finished partition");
		}

		Element e = null;
		Set<Var> varE = null;
		for(Element element : c.getAvailableQueryElements()) {
			varE = ElementUtils.mentioned(element);
			if(c.connected(varE)) {
				logger.debug(varE + " connected to " + c);
				e = element;
				break;
			}

		}
		if(e != null) {
			List<Element> list = new ArrayList<>(c.getRelaxQueryElements());
			list.add(e);


			MatchTreeRoot me = new MatchTreeRoot(c.getMatchTree());
			try {
				me = me.lazyJoin(e, graph, c.getConnectedVars());
			} catch(OutOfMemoryError err) {
				clusters.add(c);
				throw err;
			}

			Table piMe;
			Table ae;
			try {
				piMe = TableUtils.projection(me.getMatchSet(), c.getProj());
				if(Level.TRACE.isGreaterOrEqual(logger.getLevel())) {
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					ResultSet rs = piMe.toResultSet();
					ResultSetFormatter.out(baos, rs);
					logger.trace(baos.toString());
				}
				ae = TableUtils.simpleJoin(c.getAnswers(), piMe);
				if(Level.TRACE.isGreaterOrEqual(logger.getLevel())) {
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					ResultSetFormatter.out(baos, ae.toResultSet());
					logger.trace(baos.toString());
				}
			} catch(OutOfMemoryError err) {
				clusters.add(c);
				throw err;
			}
			int extensionDistance = piMe.size();


			Cluster Ce = new Cluster(c, me, ae, extensionDistance, getNextClusterId());
			Ce.move(e, varE);

			Cluster CeOpp = new Cluster(c, c.getMatchTree(), TableUtils.difference(c.getAnswers(), ae), c.getExtensionDistance(), getNextClusterId());
			CeOpp.relax(e, graph, depth);

			boolean ceEmpty = Ce.noAnswers();
			boolean ceOppEmpty = CeOpp.noAnswers();
			if(!ceEmpty) {
				clusters.add(Ce);
				if(Level.TRACE.isGreaterOrEqual(logger.getLevel())) logger.trace("Ce kept :" + Ce);
			}
			if(!ceOppEmpty) {
				clusters.add(CeOpp);
				if(Level.TRACE.isGreaterOrEqual(logger.getLevel())) logger.trace("CeOpp kept :" + CeOpp);
			}

			logger.info(clusters.size() + ":" + neighbors.size() + " - " + c.getRelaxDistance());
			return true;
		} else {
			neighbors.add(c);
			return (clusters.size() != 0);
		}
	}

	/**
	 * Applies the Partition algorithm to the end
	 *
	 * @param cut AtomicBoolean to observe, when it is set to false, the algorithm stops and cuts
	 * @return 0 if the algorithm went to the end correctly, 1 if the algorithm encountered a memory limit, -1 if it encountered an unexpected error
	 */
	public int completePartitioning(AtomicBoolean cut) {
		boolean run = true;
		boolean stop = false;
		while(run && !stop) {
			try {
				run = oneStepPartitioning();
			} catch(OutOfMemoryError mem) {
				mem.printStackTrace();
				return 1;
			} catch(PartitionException e) {
				e.printStackTrace();
				return -1;
			}
			if(cut != null) {
				stop = cut.get();
			}
		}
		if(cut.get()) return 2;
		else return 0;
	}

	public void cut() {
		this.neighbors.addAll(clusters);
		clusters.clear();
	}

	public List<Cluster> furtherPartitioningcandidates() {
		List<Cluster> list = new ArrayList<>();
		for(Cluster c : neighbors) {
			Set<Element> remaining = c.getAvailableQueryElements();
			boolean finished = true;
			if(remaining.size() != 0) {
				for(Element e : remaining) {
					if(c.connected(ElementUtils.mentioned(e))) {
						finished = false;
						break;
					}
				}
			}
			if(!finished) list.add(c);
		}
		return list;
	}

	public int targetedFurtherPartitioning(Collection<Cluster> clusterCollection, AtomicBoolean cut) throws PartitionException {
		boolean remove = true;
		for(Cluster cluster : clusterCollection) {
			remove = neighbors.remove(cluster);
			if(!remove)
				throw new PartitionException("designated cluster was not in this partition : " + cluster.toString(graph));
		}
		clusters.addAll(clusterCollection);
		return completePartitioning(cut);
	}

	public int getArity() {
		return target.size();
	}

	@Override
	public String toString() {
		StringBuilder res = new StringBuilder();
		if(Level.DEBUG.isGreaterOrEqual(logger.getLevel())) {
			res.append("Keys :\n").append(graph.getKeys()).append("\n");
		}
		res.append("\t\t").append(neighbors.size()).append(" Clusters :\n");
		PriorityQueue<Cluster> queue = new PriorityQueue<>(neighbors);
		while(!queue.isEmpty()) {
			res.append(queue.poll().toString(graph)).append("\n\n");
		}
		return res.toString();
	}

	@Override
	public String toJson() {
		StringBuilder res = new StringBuilder();
		StringBuilder uris = new StringBuilder();
		for(String uri: this.target) {
			if(uris.toString().equals("")) {
				uris.append("[");
			} else {
				uris.append(",");
			}
			uris.append("\"").append(uri).append("\"");
		}
		uris.append("]");
		res.append("{\n\"target\":").append(uris.toString()).append(",\n");
		res.append("\"clusters\":[\n\t");
		PriorityQueue<Cluster> queue = new PriorityQueue<>(neighbors);
		while(!queue.isEmpty()) {
			res.append("\t").append(queue.poll().toJson(graph).replaceAll("\n", "\n\t").replaceAll("\"\",", ""));
			res.append(",\n");
		}
		queue = new PriorityQueue<>(this.clusters);
		while(!queue.isEmpty()) {
			res.append("\t").append(queue.poll().toJson(graph).replaceAll("\n", "\n\t").replaceAll("\"\",", ""));
			if(!queue.isEmpty()) res.append(",\n");
		}
		res.append("]\n}");
		return res.toString();
	}

}
