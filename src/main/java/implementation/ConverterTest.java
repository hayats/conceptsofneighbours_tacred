package implementation;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import implementation.utils.CollectionsModel;
import org.apache.jena.rdf.model.Model;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import implementation.tacred.TacredModel;

/**
 * @author Hugo Ayats
 */
public class ConverterTest
{
	/**
	 * Mostly used for tests
	 */
	public static void main(String[] args) throws FileNotFoundException {
		if(args.length > 2 && args[2].equals("-d")) {
			database_test(args[0], args[1]);
		} else {
			sentence_test(args[0], args[1]);
		}
	}

	/**
	 * Convert a tacred-formated sentence in a JSON file into a RDF model exported in Turtle format.
	 *
	 * @param input_path path of the JSON input file.
	 * @param output_path path of the TTL output file.
	 */
	private static void sentence_test(String input_path, String output_path) throws FileNotFoundException {
		FileInputStream inputStream = new FileInputStream(input_path);
		JSONObject json_sentence = new JSONObject(inputStream);

		TacredModel converter = new TacredModel();
		converter.importTacredSentence(json_sentence, true, false, false);
		CollectionsModel graph = converter.getModel();

		FileOutputStream outputStream = new FileOutputStream(output_path);
		graph.getModel().write(outputStream, "TTL");
	}

	/**
	 * Convert a tacred-formated sentence database in a JSON file into a RDF model exported in Turtle format.
	 *
	 * @param input_path path of the JSON input file.
	 * @param output_path path of the TTL output file.
	 */
	private static void database_test(String input_path, String output_path) throws FileNotFoundException {
		System.out.println("Reading database...");
		FileInputStream inputStream = new FileInputStream(input_path);
		JSONArray json_data = new JSONArray(new JSONTokener(inputStream));

		TacredModel converter = new TacredModel();
		converter.importTacredDatabase(json_data, true, false, 0, true);
		Model graph = converter.getModel().getModel();

		System.out.println("Writing RDF graph in file...");
		FileOutputStream outputStream = new FileOutputStream(output_path);
		graph.write(outputStream, "TTL");
	}
}
