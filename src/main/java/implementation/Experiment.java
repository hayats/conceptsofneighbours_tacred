package implementation;

import implementation.algorithms.Partition;
import implementation.tacred.RelationExtractionModel;
import implementation.utils.TacredUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Experiment {
	public static final int MAX_DEPTH = 100;
	public static final int COMPUTATION_TIME = 3;
	public static final boolean PRUNED = true;
	private static final boolean VERBOSE = true;
	private static final boolean ExcludeNegativesTest = true;
	private static final boolean ExcludeNegativesTraining = true;

	public static void main(String[] args) throws IOException, InterruptedException {
		NeighborsImplementation.myLogsLevels("off");

		String trainFile = args[0];
		String testFile = args[1];
		String outputDir = args[2];
		String goldFile = args[3];
		String guessFile = args[4];

		FileWriter goldWriter = new FileWriter(goldFile);
		FileWriter guessWriter = new FileWriter(guessFile);

		RelationExtractionModel model = new RelationExtractionModel(Experiment.PRUNED,Experiment.VERBOSE,Experiment.ExcludeNegativesTraining,Experiment.ExcludeNegativesTest,Experiment.MAX_DEPTH,Experiment.COMPUTATION_TIME);

		System.out.println("Importing training database...");
		model.importTacredDatabase(new JSONArray(new JSONTokener(new FileInputStream(trainFile))), true);

		System.out.println("\nImporting testing database...");
		model.importTacredDatabase(new JSONArray(new JSONTokener(new FileInputStream(testFile))), false);

		System.gc();

		List<String> testPairs = model.getTestPairList();
		int nbTest = testPairs.size();

		System.out.println("\nComputing the concepts and scores...");

		int j = 0;
		for(int i = 0; i < nbTest; i++) {
			j++;
			if(j == 10) {
				System.gc();
				j = 0;
			}

			String id = testPairs.get(i);

			System.out.printf("[%d/%d] Sentence id: %s\n", i + 1, nbTest, id);
			JSONObject results = model.processTestExample(id);

			String gold = results.getString("gold");
			String guess = results.getString("guess");
			System.out.printf("Gold: %s\tGuess: %s\n", gold, guess);
			goldWriter.write(gold + "\n");
			guessWriter.write(guess + "\n");

			FileWriter fw = new FileWriter(String.format("%s/%s.json", outputDir, id));
			fw.write(results.toString());
			fw.close();
		}

		goldWriter.close();
		guessWriter.close();

	}
}
