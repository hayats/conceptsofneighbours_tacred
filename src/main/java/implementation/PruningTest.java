package implementation;

import implementation.tacred.TacredModel;
import org.json.JSONArray;
import org.json.JSONTokener;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class PruningTest {
	public static void main(String[] args) throws FileNotFoundException {
		String database_path = args[0];

		NeighborsImplementation.myLogsLevels("off");

		System.out.println("Importing pruned & unfiltered database...");
		TacredModel model = new TacredModel();
		model.importTacredDatabase(new JSONArray(new JSONTokener(new FileInputStream(database_path))), true, true, 0, false);

		System.out.printf("Pruned version:\n" +
				"Nb of sentences: %d\n" +
				"Nb triples: %d\n", model.getNbrImportedTrainingSentences(), model.getModel().getModel().size());

		System.out.println("Importing pruned & filtered database...");
		model = new TacredModel();
		System.gc();
		model.importTacredDatabase(new JSONArray(new JSONTokener(new FileInputStream(database_path))), true, true, 1, false);

		System.out.printf("Pruned & filtered version:\n" +
				"Nb of sentences: %d\n" +
				"Nb triples: %d\n", model.getNbrImportedTrainingSentences(), model.getModel().getModel().size());

		System.out.println("Importing unpruned & filtered database...");
		model = new TacredModel();
		System.gc();
		model.importTacredDatabase(new JSONArray(new JSONTokener(new FileInputStream(database_path))), true, false, 1, false);

		System.out.printf("Unpruned & filtered version:\n" +
				"Nb of sentences: %d\n" +
				"Nb triples: %d\n", model.getNbrImportedTrainingSentences(), model.getModel().getModel().size());

		System.out.println("Importing complete database...");
		model = new TacredModel();
		System.gc();
		model.importTacredDatabase(new JSONArray(new JSONTokener(new FileInputStream(database_path))), true, false, 0, false);

		System.out.printf("Complete version:\n" +
				"Nb of sentences: %d\n" +
				"Nb triples: %d\n", model.getNbrImportedTrainingSentences(), model.getModel().getModel().size());
	}
}
