package implementation.approaches;

import org.json.JSONArray;
import org.json.JSONTokener;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class MethodRunner {
	public static void main(String[] args) throws IOException {
		JSONArray trainingDataset = new JSONArray(new JSONTokener(new FileInputStream(args[0])));
		JSONArray testDataset = new JSONArray(new JSONTokener(new FileInputStream(args[1])));
		FileWriter guessWriter = new FileWriter(args[2]);
		FileWriter goldWriter = new FileWriter(args[3]);

		AbstractApproach model = new StatsOnTypesBaseline(true, false, false);
		model.importDataset(trainingDataset);

		model.generatePredictions(testDataset);

		List<String> gold = model.getGold();
		List<String> guess = model.getGuess();
		for(int i=0; i<gold.size(); i++) {
			goldWriter.write(gold.get(i) + "\n");
			guessWriter.write(guess.get(i) + "\n");
		}
		goldWriter.close();
		guessWriter.close();

		model.printModel();
	}
}
