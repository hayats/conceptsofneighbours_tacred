package implementation.approaches;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractApproach {
	private final boolean verbose;
	private final boolean excludeNegative;
	private final List<String> gold = new ArrayList<>();
	private final List<String> guess = new ArrayList<>();

	public AbstractApproach(boolean verbose, boolean excludeNegative) {
		this.verbose = verbose;
		this.excludeNegative = excludeNegative;
	}

	public abstract void importExample(JSONObject sentence);

	public void importDataset(JSONArray trainingDataset) {
		if(verbose) {
			System.out.printf("Importing training dataset (%d examples)...\n\n", trainingDataset.length());
		}

		for(int i = 0; i < trainingDataset.length(); i++) {
			JSONObject sentence = trainingDataset.getJSONObject(i);

			if(verbose) {
				System.out.printf("Importing sentence %s (%d/%d)\n", sentence.getString("id"), i + 1, trainingDataset.length());
			}
			this.importExample(trainingDataset.getJSONObject(i));
		}
	}

	public abstract String makePrediction(JSONObject sentence);

	public void generatePredictions(JSONArray testDataset) {
		if(this.verbose) {
			System.out.printf("Processing test dataset (%d examples)...\n\n", testDataset.length());
		}

		for(int i = 0; i < testDataset.length(); i++) {
			JSONObject sentence = testDataset.getJSONObject(i);
			String gold = sentence.getString("relation");

			if(verbose) {
				System.out.printf("Processing example %s (%d/%d)\n", sentence.getString("id"), i + 1, testDataset.length());
			}

			if(!this.excludeNegative || !gold.equals("no_relation")) {
				this.gold.add(gold);
				this.guess.add(this.makePrediction(sentence));
			}
		}
	}

	public List<String> getGold() {
		return this.gold;
	}

	public List<String> getGuess() {
		return this.guess;
	}

	public abstract void printModel();
}
